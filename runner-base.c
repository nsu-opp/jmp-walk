#include <stdio.h>
#include <stdlib.h>
#include <x86intrin.h>
#include <stdint.h>
#include <inttypes.h>

#ifdef _MSC_VER
#include <intrin.h>
#else
#include <x86intrin.h>
#endif


void __attribute__ ((noinline)) walk() {
    goto exit;
    exit:
    return;
}

size_t parse_n(int argc, char **argv) {
    if (argc != 2) {
        printf("Usage: %s N\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    return strtoull(argv[1], NULL, 10);
}

uint64_t min(uint64_t a, uint64_t b) {
    return a < b ? a : b;
}

// gcc -m64 -masm=att -o asm-main.s -S .\main.c
// gcc -m64 .\asm-main.s

int main(int argc, char **argv) {
    const size_t n = parse_n(argc, argv);
    const uint64_t repeats = 50;
    const uint64_t n_min = 10;

    uint64_t min_cycles = UINT64_MAX;

    for (size_t i = 0; i < n_min; i++) {
        uint64_t start_cycles = __rdtsc();
        for (size_t j = 0; j < repeats; j++) {
            walk();
        }
        const uint64_t  total_cycles = (__rdtsc() - start_cycles) / n / repeats;
        min_cycles = min(min_cycles, total_cycles);
    }

    printf("min_cycles=%"PRIu64 "\n", min_cycles);
    return EXIT_SUCCESS;
}
