#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <assert.h>


#define HEADER_PATH "asm-header.asm.part"
#define FOOTER_PATH "asm-footer.asm.part"

#define RESULT_PATH "test-instance.s"


#define CHECK_FILE(path, f)  do { if (NULL == f) {perror(path); exit(EXIT_FAILURE);} } while (0)


void file_copy_to(const char *path, FILE *dst) {
    FILE *src = fopen(path, "r");
    CHECK_FILE(path, src);

    int c;

    while (EOF != (c = fgetc(src))) {
        fputc(c, dst);
    }

    fclose(src);
}


int rand_range(int lower_inclusive, int upper_exclusive) {
    return lower_inclusive + rand() / (RAND_MAX / (upper_exclusive - lower_inclusive) + 1); // NOLINT
}

void swap(int *a, int *b) {
    int tmp = *a;
    *a = *b;
    *b = tmp;
}

int *sattolo_cycle(int *array, int size) {
    if (NULL == array) {
        fprintf(stderr, "array is NULL\n");
        return NULL;
    }

    int i = size;
    while (i > 1) {
        i -= 1;
        int j = rand_range(0, i);
        swap(array + i, array + j);
    }

    return array;
}

int *array_new(int size) {
    if (size < 1) {
        fprintf(stderr, "invalid size: %d\n", size);
        return NULL;
    }

    int *array = (int *) malloc(sizeof(*array) * size);
    if (NULL == array) {
        perror("cannot allocate memory");
        return NULL;
    }

    return array;
}

int *array_fill_indices(int *array, int size) {
    if (NULL == array) {
        fprintf(stderr, "array is NULL\n");
        return NULL;
    }

    for (int i = 0; i < size; i += 1) {
        array[i] = i;
    }

    return array;
}

int *array_fill_random(int *array, int size) {
    if (NULL == array) {
        fprintf(stderr, "array is NULL\n");
        return NULL;
    }

    return sattolo_cycle(array_fill_indices(array, size), size);
}

size_t parse_n(int argc, char **argv) {
    if (argc != 2) {
        printf("Usage: %s N\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    return strtoull(argv[1], NULL, 10);
}


int main(int argc, char **argv) {
    srand(10);  // NOLINT

    const size_t n = parse_n(argc, argv);

    FILE *result = fopen(RESULT_PATH, "w+");
    CHECK_FILE(RESULT_PATH, result);

    file_copy_to(HEADER_PATH, result);

    int *indices = array_fill_random(array_new((int) n), (int) n);
    assert(NULL != indices && "Buy more RAM lol");

//    int k = 0;
    for (int i = 0; i < n; i++) {
//        k = indices[k];
        fprintf(result, ".generated_%d:\n", i);
        if (0 == indices[i]) {
            fprintf(result, "\tjmp .generated_ret\n");
        } else {
            fprintf(result, "\tjmp .generated_%d\n", indices[i]);
        }
    }
    free(indices);
    fprintf(result, ".generated_ret:\n");
    // 	leaq	.L.str(%rip), %rcx
    // 	leaq	walk(%rip), %rdx
    //	leaq	.LBB0_1(%rip), %r8
    //	movq    %r8, %rax
    //	subq    %rdx, %rax
    //	movq    %rax, %rdx
    //	callq	printf
//    fprintf(result, "\tleaq\t.generated_0(%%rip), %%rdx\n");
//    fprintf(result, "\tleaq\t.generated_ret(%%rip), %%r8\n");
//    fprintf(result, "\tmovq\t%%r8, %%rax\n");
//    fprintf(result, "\tsubq\t%%rdx, %%rax\n");
//    fprintf(result, "\tleaq\t.L.str.2(%%rip), %%rcx\n");
//    fprintf(result, "\tmovq\t%%rax, %%rdx\n");
//    fprintf(result, "\tcallq\tprintf\n");

    file_copy_to(FOOTER_PATH, result);

    fclose(result);

    printf("written %"PRIu64" instructions to %s\n", n, RESULT_PATH);

//    return EXIT_SUCCESS;

    system("C:\\DKs\\llvm-mingw-20231017-msvcrt-i686\\bin\\gcc.exe -m64 .\\test-instance.s");
    char cmd[1024];
    snprintf(cmd, sizeof(cmd), ".\\a.exe %"PRIu64"\n", n);
    system(cmd);

    return EXIT_SUCCESS;
}
